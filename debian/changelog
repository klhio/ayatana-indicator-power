ayatana-indicator-power (22.9.3-1ubports1) UNRELEASED; urgency=medium

  [ Guido Berhoerster ]
  * Merge version 22.9.3-1 from Debian unstable.

  [ Robert Tari ]
  * d/patches: Add 0001_fix-flashlight.patch. Fix flashlight toggle in Lomiri.
  * d/patches: Add 0002_fix-default-icon.patch. Show a default icon in case of
    no battery in Lomiri.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Thu, 02 Feb 2023 16:34:24 +0100

ayatana-indicator-power (22.9.3-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 13 Nov 2022 10:12:30 +0100

ayatana-indicator-power (22.9.2-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 12 Nov 2022 22:10:06 +0100

ayatana-indicator-power (22.9.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #1015055).
  * debian/control:
    + Bump Standards-Version: to 4.6.1. No changes needed.
  * debian/copyright:
    + Update copyright attributions.
    + Update auto-generated copyright.in file.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 02 Oct 2022 13:13:09 +0200

ayatana-indicator-power (22.2.0-1ubports1) focal; urgency=medium

  * Merge version 22.2.0-1 from Debian unstable.
  * debian/ubports.source_location: update location for 22.2.0

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Mon, 07 Mar 2022 19:25:54 +0000

ayatana-indicator-power (22.2.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Add B-Ds: lomiri-sounds and lomiri-schemas.
  * debian/rules:
    + Add build flag -DENABLE_LOMIRI_FEATURES.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 20 Feb 2022 15:54:35 +0100

ayatana-indicator-power (2.2.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop 0001_lomiri-url-dispatcher.patch. Replaced otherwise in this
      upstream release.
  * debian/copyright:
    + Update auto-generated copyright.in reference file.
    + Update copyright attributions.
    + Drop comment for po/*.po. Not applicable anymore.
  * debian/rules:
    + Adjust for upstream release 2.2.0.
  * debian/control:
    + Bump Standards-Version: to 4.6.0. No changes needed.
    + Drop B-D on liblomiri-url-dispatcher-dev.
    + Add B-D on libayatana-common-dev instead.
    + Update S: field (for XFCE, for Budgie).
  * debian/upstream/metadata:
    + Update points of contact, put UBports Foundation in Donation: field.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 18 Nov 2021 15:49:02 +0100

ayatana-indicator-power (2.1.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Add 0001_lomiri-url-dispatcher.patch. Port to Lomiri URL Dispatcher.
  * debian/control:
    + Bump Standards-Version: to 4.5.1. No changes needed.
    + Add B-D: liblomiri-url-dispatcher-dev.
    + Fix capitalization of the word 'Xfce'.
  * debian/watch:
    + Update format version to 4.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 03 Feb 2021 11:21:20 +0100

ayatana-indicator-power (2.1.1-2) unstable; urgency=medium

  * debian/control:
    + Add Debian UBports Team to Uploaders: field.
    + Mention desktop envs that support system indicators in LONG_DESCRIPTION.
    + Typo fix in LONG_DESCRIPTION.
    + Drop alternative B-D on google-mock. Add B-D: pkg-config.
    + Bump versioned B-D on cmake-extras to (>= 1.5-5~).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 07 Nov 2020 15:16:13 +0100

ayatana-indicator-power (2.1.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Add B:/R: relation for indicator-power.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 10 Sep 2020 22:18:46 +0200

ayatana-indicator-power (2.1.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Versioned D on ayatana-indicator-common (>= 0.8.0).
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 18 Aug 2020 09:37:02 +0200

ayatana-indicator-power (2.0.95-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.5.0. No changes needed.
  * debian/{control,compat}:
    + Switch to debhelper-compat notation. Bump to DH compat level version 13.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 25 Jul 2020 13:55:03 +0200

ayatana-indicator-power (2.0.94-2) unstable; urgency=medium

  * debian/control:
    + Drop from B-D: python:any. (Closes: #936175). In fact, a totally
      superfluous B-D.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 25 Nov 2019 20:50:35 +0100

ayatana-indicator-power (2.0.94-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/control:
    + Downgrade from R to S (ayatana-indicator-power): all control-center
      and power-manager relations. They pull in by far too many other
      packages.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 21 Nov 2019 11:29:37 +0100

ayatana-indicator-power (2.0.93-4) unstable; urgency=medium

  * debian/control:
    + Bump Standards-Version: to 4.4.1. No changes needed.
    + Add Rules-Requires-Root: field and set it to "no".
    + Drop from R (ayatana-indicator-power): gnome-control-center.
      Not supporting Ayatana System Indicators anymore.
    + Add to R (ayatana-indicator-power): mate-control-center.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 20 Nov 2019 15:37:54 +0100

ayatana-indicator-power (2.0.93-3) unstable; urgency=medium

  * debian/control:
    + Update Vcs-*: fields. Packaging Git has been migrated to salsa.debian.org.
    + Re-arrange Recommends: field. Move power related tools to a separate line,
      add R gnome-power-statistics. (Closes: #895479).
    + Bump Standards-Version: to 4.2.0. No changes needed.
  * debian/upstream/metadata:
    + Add file. Be compliant with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 13 Aug 2018 22:42:38 +0200

ayatana-indicator-power (2.0.93-2) unstable; urgency=medium

  * debian/patches:
    + Add 0001_enum-in-gschemas-unique.patch. Allow simultaneous installation of
      ayatana-indicator-power and indicator-power.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 20 Mar 2018 15:18:11 +0100

ayatana-indicator-power (2.0.93-1) unstable; urgency=medium

  * New upstream release.
    - Fix dependency issue in unit tests. (Closes: #892091, #878093).
    - Drop unused requirement for gudev-1.0.
  * debian/control:
    + Drop from B-D: libgudev-1.0-dev. Not required any more. (Closes: #878094).
  * debian/rules:
    + Use more condensed NEWS file as upstream changelog.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Fri, 16 Mar 2018 16:13:01 +0100

ayatana-indicator-power (2.0.92-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop 0001-systemd-unit-file-Only-build-install-if-systemd-is-a.patch.
      Applied upstream.
  * debian/control:
    + Bump Standards-Version: to 4.1.3. No changes needed.
  * debian/copyright:
    + Use secure URI for copyright format reference.
    + Update copyright attributions.
    + Update auto-generated copyright.in reference template.
  * debian/watch:
    + Use secure URL to obtain upstream sources.
  * debian/{control,compat}:
    + Bump DH compat level to 11.
  * debian/rules:
    + Migrate from dh_install --fail-missing to dh_missing --fail-missing.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 28 Feb 2018 21:18:54 +0100

ayatana-indicator-power (2.0.91-2) unstable; urgency=medium

  * Upload to unstable.

  * debian/patches:
    + Add 0001-systemd-unit-file-Only-build-install-if-systemd-is-a.patch.
      Only build+install systemd unit file if systemd is available at
      build-time.
  * debian/control:
    + Mark B-Ds libgudev-1.0-dev and systemd as available on linux-any.
    + Bump Standards-Version: to 4.1.1. No changes needed.
  * debian/{control,copyright,watch}:
    + Update to new upstream URL.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 07 Oct 2017 21:52:22 +0200

ayatana-indicator-power (2.0.91-1) experimental; urgency=medium

  * Initial upload to Debian. (Closes: #864394).

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 08 Jun 2017 00:38:33 +0200
